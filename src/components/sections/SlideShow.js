import React from "react";
import {Carousel,Container} from "react-bootstrap";
const SlideShow = () => {
    return (
      <Container style={{marginTop:"30px"}}>
      <Carousel controls={false} fade={true}>
      <Carousel.Item interval={2000}>
        <img  style={{height:"450px"}}
          className="d-block w-100"
          src="http://3.142.146.250:16000/files/8136d3ac-6800-4655-ac0b-aa65b841fee9.jpg"
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img  style={{height:"450px"}}
          className="d-block w-100"
          src="http://3.142.146.250:16000/files/faf838e5-aeaa-4110-8837-7eaac538e592.jpg"
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img  style={{height:"450px"}}
          className="d-block w-100"
          src="http://3.142.146.250:16000/files/3995ea7d-b9f2-4177-9d0f-c6e57dfbcf74.jpg"
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
    </Container>
        );
    };
    export default SlideShow;