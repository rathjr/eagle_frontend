import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import Image from '../../elements/Image';

const Logo = ({
  className,
  ...props
}) => {

  const classes = classNames(
    'brand',
    className
  );

  return (
    <div
      {...props}
      className={classes}
    >
      <h1 className="m-0">
        <Link to="/">
          <Image
            src="http://3.142.146.250:16000/files/11360298-f132-4d1b-8932-30c5b1015624.png"
            alt="Open"
            width={45}
            height={45} />
        </Link>
      </h1>
    </div>
  );
}

export default Logo;