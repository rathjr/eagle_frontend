import React, { useState } from "react";
import {Card, Container,Col,Row} from "react-bootstrap";
const Player = () => {
    const [list, setList] = useState([
        {
          id: "1",
          name: "Alexandro",
          number: "7",
          image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOACb9sS42X3z1bAhGa0MGkwRVsSC0EKKN1A&usqp=CAU"
        },
        {
            id: "1",
            name: "Narath",
            number: "34",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGf4LPcYJ3K0eYA8sLhMyik5ZpT42tb426kQ&usqp=CAU"
          },
          {
            id: "1",
            name: "nn",
            number: "8",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOACb9sS42X3z1bAhGa0MGkwRVsSC0EKKN1A&usqp=CAU"
          },
          {
            id: "1",
            name: "aa",
            number: "9",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOACb9sS42X3z1bAhGa0MGkwRVsSC0EKKN1A&usqp=CAU"
          },
          {
            id: "1",
            name: "df",
            number: "34",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOACb9sS42X3z1bAhGa0MGkwRVsSC0EKKN1A&usqp=CAU"
          },
          {
            id: "1",
            name: "sdf",
            number: "3",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOACb9sS42X3z1bAhGa0MGkwRVsSC0EKKN1A&usqp=CAU"
          },
      ])

      const [gk, setGk] = useState([
        {
          id: "1",
          name: "Prach",
          number: "1",
          image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOACb9sS42X3z1bAhGa0MGkwRVsSC0EKKN1A&usqp=CAU"
        },
        {
            id: "1",
            name: "Vka",
            number: "2",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGf4LPcYJ3K0eYA8sLhMyik5ZpT42tb426kQ&usqp=CAU"
          },
      ])

    return (
        <Container>
       <h3 style={{height:"60px",border:"1px solid gray",marginTop:"100px",textAlign:"center",lineHeight:"2"}}>
       Goalkeeper
        </h3>
            <Row className="container p-0 d-flex justify-content-center" >
                      {gk.map((item) => {
                        return (
                          <Col xl={4} md={4} sm={6} xs={6} style={{ display: "flex", justifyContent: "center" }}>
                    <Card style={{marginTop:"20px",width: '230px'}} bg="dark">
  <Card.Img variant="top" src={item.image} style={{ width: "100%", height: "150px" }} />
  <Card.Body style={{display:"flex",justifyContent:"center"}}>
      <span style={{textAlign:"center",color:"white"}}>{item.number}<br/><span style={{color:"#9CA9B3"}}>{item.name}</span></span>
  </Card.Body>
</Card>
                          </Col>
                        );
                      })}
                  </Row>
   
                  <h3 style={{height:"60px",border:"1px solid gray",marginTop:"40px",textAlign:"center",lineHeight:"2"}}>
                  Defender
        </h3>
            <Row className="container p-0 d-flex justify-content-center" >
                      {list.map((item) => {
                        return (
                          <Col xl={3} md={4} sm={6} xs={6} style={{ display: "flex", justifyContent: "center" }}>
                    <Card style={{marginTop:"20px",width: '230px'}} bg="dark">
  <Card.Img variant="top" src={item.image} style={{ width: "100%", height: "150px" }} />
  <Card.Body style={{display:"flex",justifyContent:"center"}}>
      <span style={{textAlign:"center",color:"white"}}>{item.number}<br/><span style={{color:"#9CA9B3"}}>{item.name}</span></span>

  </Card.Body>
</Card>
                          </Col>
                        );
                      })}
                  </Row>
   

                  <h3 style={{height:"60px",border:"1px solid gray",marginTop:"40px",textAlign:"center",lineHeight:"2"}}>
                  Midfielder
        </h3>
            <Row className="container p-0 d-flex justify-content-center" >
                      {list.map((item) => {
                        return (
                          <Col xl={3} md={4} sm={6} xs={6} style={{ display: "flex", justifyContent: "center" }}>
                    <Card style={{marginTop:"20px",width: '230px'}} bg="dark">
  <Card.Img variant="top" src={item.image} style={{ width: "100%", height: "150px" }} />
  <Card.Body style={{display:"flex",justifyContent:"center"}}>
      <span style={{textAlign:"center",color:"white"}}>{item.number}<br/><span style={{color:"#9CA9B3"}}>{item.name}</span></span>

  </Card.Body>
</Card>
                          </Col>
                        );
                      })}
                  </Row>


                  <h3 style={{height:"60px",border:"1px solid gray",marginTop:"40px",textAlign:"center",lineHeight:"2"}}>
                  Forward
        </h3>
            <Row className="container p-0 d-flex justify-content-center" >
                      {list.map((item) => {
                        return (
                          <Col xl={3} md={4} sm={6} xs={6} style={{ display: "flex", justifyContent: "center" }}>
                    <Card style={{marginTop:"20px",width: '230px'}} bg="dark">
  <Card.Img variant="top" src={item.image} style={{ width: "100%", height: "150px" }} />
  <Card.Body style={{display:"flex",justifyContent:"center"}}>
      <span style={{textAlign:"center",color:"white"}}>{item.number}<br/><span style={{color:"#9CA9B3"}}>{item.name}</span></span>

  </Card.Body>
</Card>
                          </Col>
                        );
                      })}
                  </Row>
        
        </Container>
        
        );
    };
    export default Player;